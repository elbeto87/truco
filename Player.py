import random

class Player ():

    def get_mazo(mazo):
        mazo = []
        for a in range(0, 7):
            for i in range(0, 4):
                palo = ['espada', 'basto', 'copa', 'oro']
                value = mazo.append(((a + 1), palo[i]))
        for a in range(9, 12):
            for i in range(0, 4):
                palo = ['espada', 'basto', 'copa', 'oro']
                value = mazo.append(((a + 1), palo[i]))
        return mazo

    def splitting_cards(player1, player2, mazo):
        for a in range(0, 3):
            random.shuffle(mazo)
            player1.append(mazo.pop())
        for a in range(0, 3):
            random.shuffle(mazo)
            player2.append(mazo.pop())
        return

    def cards_dealt(cards):
        print('*' * 50)
        print("\n")
        print("""    {} de {} || {} de {} || {} de {}""".format(cards[0][0], cards[0][1], cards[1][0], cards[1][1],
                                                     cards[2][0], cards[2][1]))
        print("\n")
        print('*' * 50)

    def calcular_envido(envido):
        valor_final = 0
        valor_guardado = 0
        a = 0
        for a in range(0, 2):
            if (envido[a][1] == envido[2][1]):
                if envido[a][0] < 10 and envido[2][0] < 10:
                    valor_final = 20 + (envido[a][0] + envido[2][0])
                    if valor_final > valor_guardado:
                        valor_guardado = valor_final
                if envido[a][0] < 10:
                    valor_final = 20 + envido[a][0]
                    if valor_final > valor_guardado:
                        valor_guardado = valor_final
                if envido[2][0] < 10:
                    valor_final = 20 + envido[2][0]
                    if valor_final > valor_guardado:
                        valor_guardado = valor_final
        if envido[0][1] == envido[1][1]:
            if envido[0][0] < 10 and envido[1][0] < 10:
                valor_final = 20 + (envido[0][0] + envido[1][0])
                if valor_final > valor_guardado:
                    valor_guardado = valor_final
            if envido[0][0] < 10:
                valor_final = 20 + envido[0][0]
                if valor_final > valor_guardado:
                    valor_guardado = valor_final
            if envido[1][0] < 10:
                valor_final = 20 + envido[1][0]
                if valor_final > valor_guardado:
                    valor_guardado = valor_final
        if valor_guardado != 0:
            print("El envido es de {}".format(valor_guardado))
        else:
            if envido[0][0] >= 10 and envido[1][0] >= 10 and envido[2][0] >= 10:
                valor_guardado = 0
                print("El envido es de 0".format(valor_guardado))
            else:
                lista_numeros = []
                for a in range(len(envido)):
                    lista_numeros.append(envido[a][0])
                lista_numeros.sort(reverse=True)
                print("El envido es de {}".format(max(lista_numeros, key= lambda x: x<10)))
        return

    def calcular_truco(truco):
        top4= [(1, 'espada'), (1, 'basto'), (7, 'espada'), (7, 'oro')]
        truco_final={}
        for a in range(len(truco)):
            if truco[a] not in top4:
                if truco[a][0] == 3: truco_final[truco[a]] = 10
                if truco[a][0] == 2: truco_final[truco[a]] = 9
                if truco[a][0] == 1: truco_final[truco[a]] = 8
                if truco[a][0] == 12: truco_final[truco[a]] = 7
                if truco[a][0] == 11: truco_final[truco[a]] = 6
                if truco[a][0] == 10: truco_final[truco[a]] = 5
                if truco[a][0] == 7: truco_final[truco[a]] = 4
                if truco[a][0] == 6: truco_final[truco[a]] = 3
                if truco[a][0] == 5: truco_final[truco[a]] = 2
                if truco[a][0] == 4: truco_final[truco[a]] = 1
            else:
                if truco[a] == (1, 'espada'): truco_final[truco[a]] = 14
                if truco[a] == (1, 'basto'): truco_final[truco[a]] = 13
                if truco[a] == (7, 'espada'): truco_final[truco[a]] = 12
                if truco[a] == (7, 'oro'): truco_final[truco[a]] = 11
        return truco_final
